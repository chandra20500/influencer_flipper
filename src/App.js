import React from 'react';
import Demo from './influencer';
import bb from './bb.jpg';
import carry from './carry.jpg';
import pew from './pew.jpg';
import Menu from './menu.js';
import byn from './byn.jpg';
import './App.css';
/**/
const Influencer = (props) => {
  const influencer = [
    {
      id : 1,
      image : byn,
      type : "couple",
      name : "bhuvan bam",
      phone: "9455841483",
      email: "jhabrajeshc@gmail.com",
      youtube_follower : "10M",
      twitter_follower : "2M",
      facebook_follower : "1M",
      instagram_follower : "5M",
      linkedin_follower : "1.7K",
      updated_by : "sabista 13-09-2019",
      added_by : "sabista 13-09-2019"
    },
    {
      id : 1,
      image : pew,
      type : "couple",
      name : "bhuvan bam",
      phone: "9455841483",
      email: "jhabrajeshc@gmail.com",
      youtube_follower : "10M",
      twitter_follower : "2M",
      facebook_follower : "1M",
      instagram_follower : "5M",
      linkedin_follower : "1.7K",
      updated_by : "sabista 13-09-2019",
      added_by : "sabista 13-09-2019"
    },
    {
      id : 1,
      image : carry,
      type : "couple",
      name : "bhuvan bam",
      phone: "9455841483",
      email: "jhabrajeshc@gmail.com",
      youtube_follower : "10M",
      twitter_follower : "2M",
      facebook_follower : "1M",
      instagram_follower : "5M",
      linkedin_follower : "1.7K"
    },
    {
      id : 1,
      image : bb,
      type : "couple",
      name : "bhuvan bam",
      phone: "9455841483",
      email: "jhabrajeshc@gmail.com",
      youtube_follower : "10M",
      twitter_follower : "2M",
      facebook_follower : "1M",
      instagram_follower : "5M",
      linkedin_follower : "1.7K",
      updated_by : "sabista 13-09-2019",
      added_by : "sabista 13-09-2019"
    },
    {
      id : 2,
      image : carry,
      type : "single",
      name : "carryminati",
      phone: "7047033887",
      email: "brajesh0022@gmail.com",
      youtube_follower : "5M",
      twitter_follower : "0.2M",
      facebook_follower : "0.1M",
      instagram_follower : "0.5M",
      linkedin_follower : "2K",
      updated_by : "sabista 13-09-2019",
      added_by : "sabista 13-09-2019"
    },
    {
      id : 3,
      image : pew,
      type : "single",
      name : "BYN - Be you Nick",
      phone: "7985430886",
      email: "brajesh0022@gmail.com",
      youtube_follower : "1M",
      twitter_follower : "0.3M",
      facebook_follower : "0.1M",
      instagram_follower : "0.5M",
      linkedin_follower : "20K",
      updated_by : "sabista 13-09-2019",
      added_by : "sabista 13-09-2019"
    }
  ]

  const Influencerlist = influencer.map( (s,i) => {
    return <Demo 
              image = {influencer[i].image}
              type = {influencer[i].type}
              name = {influencer[i].name}
              phone= {influencer[i].phone}
              email= {influencer[i].email}
              youtube_follower= {influencer[i].youtube_follower}
              twitter_follower={influencer[i].twitter_follower}
              facebook_follower={influencer[i].facebook_follower}
              instagram_follower={influencer[i].instagram_follower}
              linkedin_follower={influencer[i].linkedin_follower}
              updated_by={influencer[i].updated_by}
              added_by={influencer[i].added_by}
          />
  } )

  return (
    <div>
      <Menu />
        <div className="searchbar"> 
        {/* <i class="icon ion-ios-arrow-round-back"></i> */}
        <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
          <input type="search" className="search" placeholder="Search influencer" />
          <input type="button" className="import_csv" value="Import CSV"/>
          <input type="button" className="add_influencer" value="+ New Influencer"/>
        </div>
      <div className="data">
        <div className="filter">
          Filter<i class="fa fa-sliders" aria-hidden="true"></i>
        </div>
        {Influencerlist}
      </div>
      
      
      
    </div>
  );
}

export default Influencer;

