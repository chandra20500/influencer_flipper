import React, { Component } from 'react';
import Boomlet from './assets/boom.JPG';
import './stylesheets/header.css';

class Header extends Component{
    render(){
        return(
        <div className="container">
            <i class="icon ion-md-menu" id="menu"></i>
            <img src={Boomlet} className="logo" />
                <div className="toolbar">
                    <i class="icon ion-ios-arrow-round-back"></i><p>Create Proposal</p>
                    <button className="preview" placeholder="preview">preview</button>
                    <button className="save" placeholder="save">Save & Create</button>
                </div>
        </div>
        )
    }
}

export default Header;