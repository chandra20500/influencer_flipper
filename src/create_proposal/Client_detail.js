import React,{Component} from 'react';
import './stylesheets/clint_detail.css';

class Client_detail extends Component{
    render(){
        return(
            <div className="Client_detail"> 
                <div className="heading">
                    <p>
                        CLIENT DETAIL
                    </p>
                </div>
                <div className="blocks">
                <div className="influencer_category">
                    <p className="title">
                        Influencer Category
                    </p>
                    <p className="value">
                        {this.props.influencer_category}
                        
                    </p>
                </div>
                <div className="influencer_category">
                    <p className="title">
                        Influencer type
                    </p>
                    <p className="value">
                        {this.props.influencer_type}
                    </p>
                </div>
                <div className="influencer_category">
                    <p className="title">
                        Platform
                    </p>
                    <p className="value">
                        {this.props.platform}
                    </p>
                </div>
                <div className="influencer_category">
                    <p className="title">
                        Deliverables
                    </p>
                    <p className="value">
                        {this.props.deliverables}
                    </p>
                </div>
                <div className="influencer_category">
                    <p className="title">
                        Co-ordinates
                    </p>
                    <p className="value">
                        {this.props.co_ordinates}
                    </p>
                </div>
                <div className="influencer_category">
                    <p className="title">
                        Campain Budget
                    </p>
                    <p className="value">
                        {this.props.campain_budget}
                    </p>
                </div>
                <div className="influencer_category">
                    <p className="title">
                        Campain Duration
                    </p>
                    <p className="value">
                        {this.props.campain_duration}
                        Lifestyle
                    </p>
                </div>
                </div>
                <div className="breif">
                    <p className="clients_breif">
                        Client's Breif
                    </p>
                    <p className="enter_detail">
                        Enter Detail
                    </p>
                </div>
            </div>
        )
    }
}

export  default Client_detail;