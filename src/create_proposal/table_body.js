import React,{Component} from 'react';
import './stylesheets/t_body.css'
class T_body extends Component{
    render(){
        return(
            <tr>
                <td>
                    {this.props.name}
                </td>
                <td>
                    {this.props.subscribers}
                </td>
                <td>
                    {this.props.language}
                </td>
                <td>
                    {this.props.avg}
                </td>
                <td>
                    {this.props.avg2}
                </td>
                <td>
                    {this.props.engagment}
                </td>
                <td>
                    {this.props.followers}
                </td>
                <td>
                    {this.props.deliverables}
                </td>
                <td>
                    {this.props.packaged}
                </td>
                <td>
                    <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;
                    <i class="fa fa-times" aria-hidden="true"></i>
                </td>
            </tr>
        )
    }
}

export default T_body;