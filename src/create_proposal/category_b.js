import React,{Component} from 'react';
import './stylesheets/category_b.css';
import ig from './assets/insta.jpg';
import T_body from './table_body.js';


const Detail = (props) => {
    const Detail_arr = [
      {
        name : "bhivan bam",
        subscribers : "15.7M",
        language : "hindi",
        avg : "300K",
        avg2 : "2,92,240",
        engagment : "0.06",
        followers : "90% real",
        deliverables : "1 video + 3 stories",
        packaged : "1,50000rs"
      },
      {
        name : "bhuvan bam",
        subscribers : "15.7M",
        language : "hindi",
        avg : "300K",
        avg2 : "2,92,240",
        engagment : "0.06",
        followers : "90% real",
        deliverables : "1 video + 3 stories",
        packaged : "1,50000rs"
      },
      {
        name : "fit tuber",
        subscribers : "15.7M",
        language : "hindi",
        avg : "300K",
        avg2 : "2,92,240",
        engagment : "0.06",
        followers : "90% real",
        deliverables : "1 video + 3 stories",
        packaged : "1,50000rs"
      },
      {
        name : "Flying beast",
        subscribers : "15.7M",
        language : "hindi",
        avg : "300K",
        avg2 : "2,92,240",
        engagment : "0.06",
        followers : "90% real",
        deliverables : "1 video + 3 stories",
        packaged : "1,50000rs"
      },
      {
        name : "Amit Badhana",
        subscribers : "15.7M",
        language : "hindi",
        avg : "300K",
        avg2 : "2,92,240",
        engagment : "0.06",
        followers : "90% real",
        deliverables : "1 video + 3 stories",
        packaged : "1,50000rs"
      }
    ]

const Table_b = Detail_arr.map( (s,i) => {
    return(
    <T_body 
        name = {Detail_arr[i].name}
        subscribers = {Detail_arr[i].subscribers}
        language = {Detail_arr[i].language}
        avg = {Detail_arr[i].avg}
        avg2 = {Detail_arr[i].avg2}
        engagment = {Detail_arr[i].engagment}
        followers = {Detail_arr[i].followers}
        deliverables = {Detail_arr[i].deliverables}
        packaged = {Detail_arr[i].packaged}
    />
    )
}
)
        return(
            <div className="cat_b">
                <p className="title_heading">
                    <strong>Category B</strong>
                </p>
                <p className="image_title">
                    <img src={ig} className="instagram_b" />
                INSTAGRAM</p>
                {/* <input type="button" className="edit" >Edit</input> */}
                <button className="edit">Edit</button>
                <button className="delete">Delete</button>

                <table className="table">
                    <th>
                        Name
                    </th>
                    
                    <th>
                        Subscribers
                    </th>
                    
                    <th>
                        Language
                    </th>
                    
                    <th>
                        Average views
                    </th>
                    <th>
                        Average views<p>(based on breakdown from you end)</p>
                    </th>                    
                    <th>
                        Engagment
                    </th>
                    <th>
                        Followers Audit
                    </th>
                    
                    <th>
                        Deliverables
                    </th>
                    
                    <th>
                        Packaged Cost
                    </th>
                    
                    <th>
                        Action
                    </th>
                    {Table_b}
                </table>
                
            </div>
         )
        }


export default Detail;