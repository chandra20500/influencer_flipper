import React,{Component} from 'react';
import './stylesheets/detail.css'
import Client_detail from './Client_detail.js';
import ID from './influencer_detail';
import Disclaimer from './disclaimer.js';
import Cat_b from './category_b.js';
import {Link} from 'react-router-dom';

const Influencer_category = "Lifestyle";
const Influencer_type = "CAT A";
const Platform = "YOU TUBE";
const Deliverables = "NTD";
const Co_ordinates = "NTD";
const Campain_budget = "12000";
const Campain_duration = "1";

class Detail extends Component{
    render(){
        return(
        <div className="asd">
            <div className="body-container">
                <div className="date" id="active_entry">
                    <p className="header" id="active">
                        Proposal date
                    </p>
                    <p className="value"> 
                        {this.props.date}
                    </p>
                </div>
                <div className="date">
                    <p className="header">
                        Brand / Agency
                    </p>
                    <p className="value"> 
                        {this.props.agency}
                    </p>
                </div>
                <div className="date">
                    <p className="header">
                        Brand name
                    </p>
                    <p className="value"> 
                        {this.props.brand}
                    </p>
                </div>
                <div className="date">
                    <p className="header">
                        Clint name
                    </p>
                    <p className="value"> 
                        {this.props.brand}
                    </p>
                </div>
                <div className="date">
                    <p className="header">
                        Strategist
                    </p>
                    <p className="value"> 
                        {this.props.strategist}
                    </p>
                </div>
                <div className="date">
                    <p className="header">
                        Contact Number
                    </p>
                    <p className="value"> 
                        {this.props.contact}
                    </p>
                </div><div className="date">
                    <p className="header">
                        Email ID
                    </p>
                    <p className="value"> 
                        {this.props.mail}
                    </p>
                </div>
            </div>
            <Client_detail 
                influencer_category={Influencer_category}
                influencer_type={Influencer_type}
                platform={Platform}
                deliverables={Deliverables}
                co_ordinates={Co_ordinates}
                campain_budget={Campain_budget}
                campain_duration={Campain_duration}
            />
            <div className="add_influencer_detail">
                <p>
                    INFLUENCER DETAILS
                </p>
                <button>
                    <Link to="/influencer">+ Add Influencer</Link>
                </button>

            </div>

        </div>
        )
    }
}

export default Detail;