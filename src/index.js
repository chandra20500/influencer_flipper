import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Proposal from './proposals_App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Proposal />, document.getElementById('root'));

serviceWorker.unregister();
