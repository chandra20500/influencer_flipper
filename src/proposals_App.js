import React from 'react';
import './create_proposal/stylesheets/App.css';
import Header from './create_proposal/header.js';
import Detail from './create_proposal/detail.js';
import App from './influencer/App.js';
import {BrowserRouter, Route ,Switch} from 'react-router-dom';
// import { render } from 'react-dom';

const Date = "19-feb-2020";
const Agency = "XYZ";
const Brand = "XYZ";
const Clint = "XYZ";
const Strategist = "XYZ";
const Contact = "7047033887";
const Mail = "XYZ";

function Proposal() {
  return (
    <BrowserRouter>
      <div>
          <Switch>
            <Route path="/influencer" component={App} />
            <Route path="/" exact component={Dabba} />
          </Switch>
    </div>
    </BrowserRouter>
  );
}

const Dabba = () => {
      return(
        <div className="main-container">
          <Header />
          <Detail 
            date={Date}
            brand={Brand}
            agency={Agency}
            clint={Clint}
            strategist={Strategist}
            contact={Contact}
            mail={Mail}
          />
        </div>
      );
}

export default Proposal;
