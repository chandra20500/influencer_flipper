import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import './stylesheets/influencer.css'
import insta from './assets/insta.jpg';
import facebook from './assets/facebook.png';
import yt from './assets/yt.png';
import tw from './assets/tw.png';
import lk from './assets/linkedin.png';

class Demo extends Component{
    render(){
        return <div className="main">           
            <div className="checkbox_influencer">
                    <input type="checkbox" id="checkbox_influencer" />
                    {/* <i class="fa fa-square-o" id="checkbox" aria-hidden="true"></i> */}
                </div>
            <div className="influencer">
                <div className="channel-img">
                    <img src={this.props.image} />
                    <p>
                        {this.props.type}
                    </p>
                </div>
                <div className="detail">
                    <div className="as">{this.props.name} </div>
                    <p> {this.props.phone} </p>
                    <p> {this.props.email} </p>
                    <button className="b1"> travel </button>
                    <button className="b1"> sports </button>
                    <button className="b1" id="sqb"> travel </button>
                </div>
                <div className="social">
                    <div className="test">
                        <img src={insta} />
                        <p>{this.props.instagram_follower}</p>
                    </div> 
                    <div className="test">
                        <img src={facebook} />
                        <p>{this.props.facebook_follower}</p>
                    </div>
                    <div className="test">
                        <img src={lk} />
                        <p>{this.props.linkedin_follower}</p>
                    </div>
                    <div className="test">
                        <img src={tw} />
                        <p>{this.props.twitter_follower}</p>
                    </div>
                    <div className="test">
                        <img src={yt} />
                        <p>{this.props.youtube_follower}</p>
                    </div>                                    
                </div>
            </div>
            <div className="edit_options">
                <div className="influencer-origin">
                    <strong>Updated by</strong> : {this.props.updated_by}
                &nbsp;&nbsp;
                    <strong>Added by</strong>: {this.props.added_by}
                </div>
                <div className="influencer-options_influencer">
                    <button className="delete_influencer"> delete </button>
                    <button className="edit_influencer"> edit </button>
                    <button className="view_influencer" id="view"> View </button>
                </div>

            </div>
        </div>
    }
}

export default Demo;