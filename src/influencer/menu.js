import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import boomlet_icon from './assets/boom.JPG';
import menu_icon from './assets/menu.png';
import back from './assets/back.png';
import './stylesheets/menu.css';


class Menu extends Component{
    render(){
        return(
            <div className="container_menu_menu">
                <div className="images_menu">
                    {/* <img src={menu_icon} alt="menu" className="menu" /> */}
                    {/* <i class="fa fa-bars" aria-hidden="true"></i> */}
                    {/* <i class="fa fa-tasks" aria-hidden="true"></i> */}
                    <i class="fa fa-align-left" aria-hidden="true" id="menu_button"></i>
                    <img src={boomlet_icon} alt="=boomlet-icon" className="boomlet_icon" />
                    <div className="sidebar">
                        <i class="icon ion-md-cube" id="inactive"></i>
                            <p>
                                overview
                            </p>
                        </div>
                    <div className="sidebar" >
                        <i class="icon ion-ios-megaphone" id="inactive"></i>
                        <p>
                            campain
                        </p>
                    </div>
                    <div className="sidebar" id="active">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                        <p>
                            influencer
                        </p>
                    </div>
                </div>
                {/* <div className="tools">
                    <i class="icon ion-md-arrow-back"></i>
                </div> */}

            </div>
            
        )
    }
}

export default Menu;